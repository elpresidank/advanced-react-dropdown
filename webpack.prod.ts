import path = require("path");
import webpack = require("webpack");
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import IgnoreEmitPlugin from "ignore-emit-webpack-plugin";
import MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
import CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
import ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

const config: webpack.Configuration = {
  mode: "production",
  devtool: "source-map",
  entry: {
    app: "./src/index.tsx",
    styles: "./src/scss/main.scss",
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "src/scss"),
        use: [MiniCssExtractPlugin.loader, "css-loader", "scss-loader"],
      },
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-react",
              "@babel/preset-typescript",
            ],
          },
        },
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: "file-loader",
        options: {
          name: "assets/img/[name].[ext]",
        },
      },
      {
        test: /\.(svg)$/,
        loader: "file-loader",
        options: {
          name: "assets/img/icons/[name].svg",
        },
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx"],
  },
  output: {
    publicPath: "",
    filename: "[name].[contenthash].js",
    path: path.resolve(__dirname, "./dist"),
  },
  plugins: [
    new CleanWebpackPlugin(),
    new ForkTsCheckerWebpackPlugin(),
    new HtmlWebpackPlugin({
      template: "src/index.html"
    }),
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash].css",
      chunkFilename: "[id].[contenthash].css",
      ignoreOrder: true,
    }),
    new IgnoreEmitPlugin(/styles.*.js$/),
  ],
  optimization: {
    moduleIds: "deterministic", // Use to keep vendor hash consistent between builds
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          name: "vendors",
          chunks: "all",
        },
        styles: {
          name: "styles",
          test: /\.css$/,
          chunks: "all",
          enforce: true,
        },
      },
    },
    minimizer: ["...", new CssMinimizerPlugin()],
  },
};

export default config;
